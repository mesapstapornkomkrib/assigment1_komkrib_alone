package HW_ALONE;

public class Pro implements Intro {
	public String price;
	public String title;
	public int id;

	public Pro(String price, String title, int id) {
		this.price = price;
		this.title = title;
		this.id = id;

	}

	@Override
	public String toString() {

		return title;
	}

	@Override
	public String getTitleCom() {
		return title;
	}

	@Override
	public String getPriceCom() {
		return price;
	}

	@Override
	public int getID() {
		return id;
	}

}
